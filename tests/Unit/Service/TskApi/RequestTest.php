<?php


namespace App\Tests\Unit\Service\TskApi;


use App\Service\TskApi\Entity\ParkingList;
use App\Service\TskApi\Request;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class RequestTest extends WebTestCase
{
    /**
     * @var Request
     */
    private $tskApiRequest;

    public function setUp()
    {
        self::bootKernel();

        $container = self::$kernel->getContainer();

        $container = self::$container;

        $serializer = self::$container->get('serializer');

        $this->tskApiRequest = new Request($serializer);
    }

    public function testInstance()
    {
        $this->assertInstanceOf(Request::class, $this->tskApiRequest);
    }

    public function testGetParkings()
    {
        $parkingList = $this->tskApiRequest->getParkings();
        $this->assertInstanceOf(ParkingList::class, $parkingList);
    }
}