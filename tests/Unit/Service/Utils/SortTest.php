<?php


namespace App\Tests\Unit\Service\Utils;

use App\Service\Utils\Sort;
use PHPUnit\Framework\TestCase;

class SortTest extends TestCase
{
    protected $expectFloatSortingASC = [
        [
            'stringValue' => 'b',
            'integerValue' => 8,
            'floatValue' => 2.5452
        ],
        [
            'stringValue' => 'a',
            'integerValue' => 2,
            'floatValue' => 12.5452
        ],
        [
            'stringValue' => 'd',
            'integerValue' => 1,
            'floatValue' => 18.12
        ],
        [
            'stringValue' => 'c',
            'integerValue' => 5,
            'floatValue' => 28.152
        ],
    ];

    protected $expectFloatSortingDESC = [
        [
            'stringValue' => 'c',
            'integerValue' => 5,
            'floatValue' => 28.152
        ],
        [
            'stringValue' => 'd',
            'integerValue' => 1,
            'floatValue' => 18.12
        ],
        [
            'stringValue' => 'a',
            'integerValue' => 2,
            'floatValue' => 12.5452
        ],
        [
            'stringValue' => 'b',
            'integerValue' => 8,
            'floatValue' => 2.5452
        ],
    ];

    protected $expectIntegerSortingASC = [
        [
            'stringValue' => 'd',
            'integerValue' => 1,
            'floatValue' => 18.12
        ],
        [
            'stringValue' => 'a',
            'integerValue' => 2,
            'floatValue' => 12.5452
        ],
        [
            'stringValue' => 'c',
            'integerValue' => 5,
            'floatValue' => 28.152
        ],
        [
            'stringValue' => 'b',
            'integerValue' => 8,
            'floatValue' => 2.5452
        ],
    ];

    protected $expectIntegerSortingDESC = [
        [
            'stringValue' => 'b',
            'integerValue' => 8,
            'floatValue' => 2.5452
        ],
        [
            'stringValue' => 'c',
            'integerValue' => 5,
            'floatValue' => 28.152
        ],
        [
            'stringValue' => 'a',
            'integerValue' => 2,
            'floatValue' => 12.5452
        ],
        [
            'stringValue' => 'd',
            'integerValue' => 1,
            'floatValue' => 18.12
        ],
    ];

    protected $expectStringSortingASC = [
        [
            'stringValue' => 'a',
            'integerValue' => 2,
            'floatValue' => 12.5452
        ],
        [
            'stringValue' => 'b',
            'integerValue' => 8,
            'floatValue' => 2.5452
        ],
        [
            'stringValue' => 'c',
            'integerValue' => 5,
            'floatValue' => 28.152
        ],
        [
            'stringValue' => 'd',
            'integerValue' => 1,
            'floatValue' => 18.12
        ],
    ];

    protected $expectStringSortingDESC = [
        [
            'stringValue' => 'd',
            'integerValue' => 1,
            'floatValue' => 18.12
        ],
        [
            'stringValue' => 'c',
            'integerValue' => 5,
            'floatValue' => 28.152
        ],
        [
            'stringValue' => 'b',
            'integerValue' => 8,
            'floatValue' => 2.5452
        ],
        [
            'stringValue' => 'a',
            'integerValue' => 2,
            'floatValue' => 12.5452
        ],
    ];

    protected $testDataArrayOfArrays = [
        [
            'stringValue' => 'a',
            'integerValue' => 2,
            'floatValue' => 12.5452
        ],
        [
            'stringValue' => 'b',
            'integerValue' => 8,
            'floatValue' => 2.5452
        ],
        [
            'stringValue' => 'c',
            'integerValue' => 5,
            'floatValue' => 28.152
        ],
        [
            'stringValue' => 'd',
            'integerValue' => 1,
            'floatValue' => 18.12
        ],
    ];

    /**
     * @var Sort
     */
    protected $sort;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->sort = new Sort();
        $this->assertInstanceOf(Sort::class, $this->sort);
    }

    public function testSortArrayOfArraysByItemValue()
    {

        $sorted = $this->sort->sortArrayOfArraysByItemValue($this->testDataArrayOfArrays, 'floatValue');
        $this->assertEquals($this->expectFloatSortingASC, $sorted);

        $sorted = $this->sort->sortArrayOfArraysByItemValue($this->testDataArrayOfArrays, 'floatValue', 'DESC');
        $this->assertEquals($this->expectFloatSortingDESC, $sorted);

        $sorted = $this->sort->sortArrayOfArraysByItemValue($this->testDataArrayOfArrays, 'integerValue');
        $this->assertEquals($this->expectIntegerSortingASC, $sorted);

        $sorted = $this->sort->sortArrayOfArraysByItemValue($this->testDataArrayOfArrays, 'integerValue', 'DESC');
        $this->assertEquals($this->expectIntegerSortingDESC, $sorted);

        $sorted = $this->sort->sortArrayOfArraysByItemValue($this->testDataArrayOfArrays, 'stringValue');
        $this->assertEquals($this->expectStringSortingASC, $sorted);

        $sorted = $this->sort->sortArrayOfArraysByItemValue($this->testDataArrayOfArrays, 'stringValue', 'DESC');
        $this->assertEquals($this->expectStringSortingDESC, $sorted);
    }

    public function testNotExistingOrderByItem()
    {
        $this->expectException(\InvalidArgumentException::class);
        $sorted = $this->sort->sortArrayOfArraysByItemValue($this->testDataArrayOfArrays, 'notExistingValue');
    }

    public function testNotExistingOrderType()
    {
        $this->expectException(\InvalidArgumentException::class);
        $sorted = $this->sort->sortArrayOfArraysByItemValue($this->testDataArrayOfArrays, 'stringValue', 'notExistOrderType');
    }
}