<?php


namespace App\Tests\Unit\Service\Utils\Coordinates;


use App\Service\Utils\Coordinates\DestinationCoordinates;
use App\Service\Utils\Coordinates\Operations;
use App\Service\Utils\Coordinates\OriginCoordinates;
use Location\Coordinate;
use PHPUnit\Framework\TestCase;
use PhpUnitsOfMeasure\Exception\UnknownUnitOfMeasure;

class OperationsTest extends TestCase
{

    public function testDistanceMeasurement()
    {
        $originCoordinates = new Coordinate(50.076155, 14.311146);

        $destinationCoordinates = new Coordinate(50.051366, 14.438199);

        $operations = new Operations();
        $this->assertInstanceOf(Operations::class, $operations);


        $countedDistance = $operations->getVincentyDistance($originCoordinates, $destinationCoordinates, 'km');
        $this->assertEquals(9.505787, $countedDistance);

        $countedDistance = $operations->getVincentyDistance($originCoordinates, $destinationCoordinates, 'm');
        $this->assertEquals(9505.787, $countedDistance);
    }

    public function testUnknownMetricUnit()
    {
        $originCoordinates = new Coordinate(50.076155, 14.311146);
        $destinationCoordinates = new Coordinate(50.051366, 14.438199);

        $operations = new Operations();
        $this->expectException(UnknownUnitOfMeasure::class);
        $countedDistance = $operations->getVincentyDistance($originCoordinates, $destinationCoordinates, 'unknownMetricUnit');
    }

    public function testNotPassingCoordinates()
    {
        $originCoordinates = new Coordinate(50.076155, 14.311146);
        $destinationCoordinates = 'w56f46wef';

        $operations = new Operations();
        $this->expectException(\TypeError::class);
        $countedDistance = $operations->getVincentyDistance($originCoordinates, $destinationCoordinates, 'unknownMetricUnit');
    }

}