<?php


namespace App\Tests\Functional;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ParkingControllerTest extends WebTestCase
{

    public function testGetClosestParkingJson()
    {
        $client = static::createClient();
        $client->request(Request::METHOD_GET, 'api/parking/closest/50.093143/14.332367');

        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertRegExp('#application\/json#', $client->getResponse()->headers->get('content-type'));
        $this->assertRegExp('#{"lat":50.06858,"lng":14.358078,"name":".+","numOfFreePlaces":\d+}#', $client->getResponse()->getContent());
    }

    public function testGetAllParkingJson()
    {
        $client = static::createClient();
        $client->request(Request::METHOD_GET, 'api/parking/all/50.093143/14.332367');

        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertRegExp('#application\/json#', $client->getResponse()->headers->get('content-type'));
    }

    public function testGetAllParkingAsCSV()
    {
        $client = static::createClient();
        $client->request(Request::METHOD_GET, 'api/parking/csv/50.093143/14.332367');

        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertRegExp('#text\/csv#', $client->getResponse()->headers->get('content-type'));
        $this->assertRegExp('#.*\.csv"$#', $client->getResponse()->headers->get('content-disposition'));
    }
}