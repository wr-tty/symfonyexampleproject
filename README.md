
# SymfonyExampleProject - Parkoviště

## API Structure

- api/parking/closest[latitiude]/[longitude] - returning closest parking as JSON
- api/parking/csv/[latitiude]/[longitude] - returning CSV file
- api/parking/all/[latitiude]/[longitude] - returning all parking as JSON

### Quick examples

- http://localhost:8000/api/parking/closest/50.093143/14.332367
- http://localhost:8000/api/parking/csv/50.093143/14.332367
- http://localhost:8000/api/parking/all/50.093143/14.332367

### Prerequisites

- Docker (optional)

### Installing

### Docker
-  pull repository
- application run command:
```
docker-compose up
```
- applicaton start runing on port 8000 - available at http://localhost:8000
- for shutdown
```
docker-compose down
```
### Standard

- check your php version
- run:
```
symfony server:start
```

## Running the tests

```
./bin/phpunit
```

### Coding style checks

```
vendor/bin/ecs check src --ansi
```

```
vendor/bin/phpstan analyse src --ansi --memory-limit 64
```

