<?php

declare(strict_types=1);

namespace App\Controller\Api;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ApiBaseController
 * @package App\Controller\Api
 * @Route("/api", name="api_")
 */
class BaseController
{
    /**
     * @Route("/", name="guidepost")
     * @return Response
     */
    public function index()
    {
        return new Response('<html><body>Api rozcestník</body></html>');
    }
}
