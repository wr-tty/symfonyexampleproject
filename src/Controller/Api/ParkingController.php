<?php

declare(strict_types=1);

namespace App\Controller\Api;

use Location\Coordinate;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ApiBaseController
 * @package App\Controller\Api
 * @Route(
 *     "/api/parking",
 *     name="api_parking_"
 * )
 */
class ParkingController extends AbstractController
{
    /**
     * @var \App\Service\ApiMapper\Response
     */
    protected $apiResponse;

    public function __construct(\App\Service\ApiMapper\Response $apiResponse)
    {
        $this->apiResponse = $apiResponse;
    }

    /**
     * @Route(
     *     "/csv/{latitude}/{longitude}",
     *     name="csv_list",
     *     methods={"GET"}
     * )
     * @ParamConverter("coordinate", class="Location\Coordinate")
     * @return Response
     */
    public function getCSVList(Coordinate $coordinate)
    {
        $parkingListCsv = $this->apiResponse->getAllParkingCSV($coordinate);
        $headers = [
            'content-type' => 'text/csv; charset=utf8',
            'content-disposition' => 'inline; filename="listParkingAll.csv"',
        ];
        return new Response($parkingListCsv, 200, $headers);
    }

    /**
     * @Route(
     *     "/all/{latitude}/{longitude}",
     *     name="list",
     *     methods={"GET"}
     * )
     * @ParamConverter("coordinate", class="Location\Coordinate")
     * @return Response
     */
    public function getList(Coordinate $coordinate)
    {
        $parkingList = $this->apiResponse->getAllParking($coordinate);
        return new JsonResponse($parkingList, Response::HTTP_OK, [], true);
    }

    /**
     * @Route(
     *     "/closest/{latitude}/{longitude}",
     *     name="closest",
     *     methods={"GET"}
     * )
     * @ParamConverter("coordinate", class="Location\Coordinate")
     * @return Response
     */
    public function getClosest(Coordinate $coordinate)
    {
        $parking = $this->apiResponse->getClosestParking($coordinate);
        return new JsonResponse($parking, Response::HTTP_OK, [], true);
    }
}
