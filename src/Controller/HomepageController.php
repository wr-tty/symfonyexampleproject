<?php

declare(strict_types=1);

namespace App\Controller;

use App\Service\TskApi\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class HomepageController
 * @package App\Controller
 * @Route("/", name="homepage_")
 */
class HomepageController
{
    /**
     * @var Request
     */
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index()
    {
        return new Response('
            <html>
                <body>
                    <h1>Parkoviště app</h1>
                </body>
            </html>
        ');
    }
}
