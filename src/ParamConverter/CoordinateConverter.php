<?php

declare(strict_types=1);

namespace App\ParamConverter;

use App\Exception\UserVisibleException;
use Location\Coordinate;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;

final class CoordinateConverter implements ParamConverterInterface
{
    public const REQUIRED_VARIABLES = ['latitude', 'longitude'];

    public function apply(Request $request, ParamConverter $paramConverter): bool
    {
        $coordinates = [];
        foreach (self::REQUIRED_VARIABLES as $requiredVariable) {
            $coordinateValue = $request->attributes->get($requiredVariable);
            if ($coordinateValue && is_numeric($coordinateValue)) {
//                $coordinates[$requiredVariable] = $coordinateValue;
                $coordinates[] = (float) $coordinateValue;
                continue;
            }
            throw new UserVisibleException('Latitude and Longitude values must be numerical.');
        }
        $request->attributes->set($paramConverter->getName(), new Coordinate(...$coordinates));
        return true;
    }

    public function supports(ParamConverter $paramConverter): bool
    {
        if ($paramConverter->getClass() === Coordinate::class) {
            return true;
        }
        return false;
    }
}
