<?php

declare(strict_types=1);

namespace App\Service\TskApi\Entity;

use Doctrine\Common\Annotations\Annotation\Required;

class ParkingList
{
    /**
     * @var string
     * @Required()
     */
    private $desc;

    /**
     * @var Parking[]
     * @Required()
     */
    private $results = [];

    public function getDesc(): string
    {
        return $this->desc;
    }

    public function setDesc(string $desc): void
    {
        $this->desc = $desc;
    }

    /**
     * @return Parking[]
     */
    public function getResults(): array
    {
        return $this->results;
    }

    /**
     * @param Parking[] $results
     */
    public function setResults(array $results): void
    {
        $this->results = $results;
    }

    public function hasResults(): bool
    {
        return count($this->results) > 0;
    }

    public function addResult(Parking $parking): void
    {
        $this->results[] = $parking;
    }

    public function removeResult(Parking $parking): bool
    {
        foreach ($this->results as $key => $parkingItem) {
            if ($parking === $parkingItem) {
                unset($this->results[$key]);
                return true;
            }
        }
        return false;
    }
}
