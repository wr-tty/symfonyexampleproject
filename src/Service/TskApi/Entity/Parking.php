<?php

declare(strict_types=1);

namespace App\Service\TskApi\Entity;

use Symfony\Component\Serializer\Annotation\Groups;

class Parking
{
    /**
     * @var int
     */
    private $gid;

    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $lastUpdated;

    /**
     * @var float
     * @Groups({"closest", "all"})
     */
    private $lat;

    /**
     * @var float
     * @Groups({"closest", "all"})
     */
    private $lng;

    /**
     * @var string
     * @Groups({"closest", "all"})
     */
    private $name;

    /**
     * @var int
     * @Groups({"closest", "all"})
     */
    private $numOfFreePlaces;

    /**
     * @var int
     */
    private $numOfTakenPlaces;

    /**
     * @var bool
     */
    private $pr = false;

    /**
     * @var int
     */
    private $totalNumOfPlaces;

    public function getGid(): int
    {
        return $this->gid;
    }

    public function setGid(int $gid): void
    {
        $this->gid = $gid;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getLastUpdated(): int
    {
        return $this->lastUpdated;
    }

    public function setLastUpdated(int $lastUpdated): void
    {
        $this->lastUpdated = $lastUpdated;
    }

    public function getLat(): float
    {
        return $this->lat;
    }

    public function setLat(float $lat): void
    {
        $this->lat = $lat;
    }

    public function getLng(): float
    {
        return $this->lng;
    }

    public function setLng(float $lng): void
    {
        $this->lng = $lng;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getNumOfFreePlaces(): int
    {
        return $this->numOfFreePlaces;
    }

    public function setNumOfFreePlaces(int $numOfFreePlaces): void
    {
        $this->numOfFreePlaces = $numOfFreePlaces;
    }

    public function getNumOfTakenPlaces(): int
    {
        return $this->numOfTakenPlaces;
    }

    public function setNumOfTakenPlaces(int $numOfTakenPlaces): void
    {
        $this->numOfTakenPlaces = $numOfTakenPlaces;
    }

    public function isPr(): bool
    {
        return $this->pr;
    }

    public function setPr(bool $pr): void
    {
        $this->pr = $pr;
    }

    public function getTotalNumOfPlaces(): int
    {
        return $this->totalNumOfPlaces;
    }

    public function setTotalNumOfPlaces(int $totalNumOfPlaces): void
    {
        $this->totalNumOfPlaces = $totalNumOfPlaces;
    }
}
