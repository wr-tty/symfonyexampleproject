<?php

declare(strict_types=1);

namespace App\Service\TskApi;

use App\Service\TskApi\Entity\ParkingList;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class Request
{
    /**
     * @var string
     */
    protected $baseUrl;

    /**
     * @var HttpClientInterface
     */
    protected $httpClient;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * TskApi constructor.
     */
    public function __construct(SerializerInterface $serializer)
    {
        $this->baseUrl = 'http://www.tsk-praha.cz/tskexport3/json';
        $this->serializer = $serializer;
    }

    /**
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getParkings(): ParkingList
    {
        $actualEndpoint = $this->baseUrl . '/parkings';
        $response = $this->createRequest(\Symfony\Component\HttpFoundation\Request::METHOD_GET, $actualEndpoint);
        $responseContent = $response->getContent();
        return $this->deserializeToParkingList($responseContent);
    }

    protected function createRequest(string $method, string $url): ResponseInterface
    {
        $httpClient = HttpClient::create();
        return $httpClient->request($method, $url);
    }

    private function deserializeToParkingList(string $responseContent): ParkingList
    {
        return $this->serializer->deserialize($responseContent, ParkingList::class, 'json');
    }
}
