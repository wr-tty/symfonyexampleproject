<?php

declare(strict_types=1);

namespace App\Service\ApiMapper\Mappings;

use App\Service\TskApi\Entity\ParkingList;
use Location\Coordinate;

final class ListParkingCSV extends AbstractParkingBase
{
    public function getApiStructure(ParkingList $parkingList, Coordinate $coordinate): string
    {
        $allParking = $this->map($parkingList, $coordinate);
        return $this->serializer->serialize($allParking, 'csv', ['groups' => 'all']);
    }
}
