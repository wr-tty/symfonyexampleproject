<?php

declare(strict_types=1);

namespace App\Service\ApiMapper\Mappings;

use App\Service\TskApi\Entity\ParkingList;
use App\Service\Utils\Coordinates\Operations;
use Location\Coordinate;
use PhpUnitsOfMeasure\Exception\NonNumericValue;
use PhpUnitsOfMeasure\Exception\NonStringUnitName;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\SerializerInterface;

final class ListParking extends AbstractParkingBase
{
    /**
     * ListParking constructor.
     */
    public function __construct(SerializerInterface $serializer, Operations $operations)
    {
        parent::__construct($serializer, $operations);
    }

    /**
     * @throws NonNumericValue
     * @throws NonStringUnitName
     * @throws ExceptionInterface
     */
    public function getApiStructure(ParkingList $parkingList, Coordinate $coordinate): string
    {
        $allParking = $this->map($parkingList, $coordinate);
        return $this->serializer->serialize($allParking, 'json', ['groups' => 'all']);
    }
}
