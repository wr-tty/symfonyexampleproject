<?php

declare(strict_types=1);

namespace App\Service\ApiMapper\Mappings;

use App\Service\TskApi\Entity\ParkingList;
use App\Service\Utils\Coordinates\Operations;
use App\Service\Utils\Sort;
use Location\Coordinate;
use Symfony\Component\Serializer\SerializerInterface;

final class ClosestParking extends AbstractParkingBase
{
    /**
     * @var Sort
     */
    private $sort;

    public function __construct(SerializerInterface $serializer, Operations $operations, Sort $sort)
    {
        parent::__construct($serializer, $operations);
        $this->sort = $sort;
    }

    public function getApiStructure(ParkingList $parkingList, Coordinate $coordinate): string
    {
        $mappedParkingList = $this->map($parkingList, $coordinate);

        $orderedParkingList = $this->sort->sortArrayOfObjectsByPropertyValue($mappedParkingList);
        $closestParking = array_shift($orderedParkingList);
        return $this->serializer->serialize($closestParking, 'json', ['groups' => 'closest']);
    }
}
