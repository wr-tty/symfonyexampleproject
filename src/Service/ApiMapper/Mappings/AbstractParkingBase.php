<?php

declare(strict_types=1);

namespace App\Service\ApiMapper\Mappings;

use App\Service\ApiMapper\Entity\Parking;
use App\Service\TskApi\Entity\ParkingList;
use App\Service\Utils\Coordinates\Operations;
use Location\Coordinate;
use PhpUnitsOfMeasure\Exception\NonNumericValue;
use PhpUnitsOfMeasure\Exception\NonStringUnitName;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

abstract class AbstractParkingBase
{
    /**
     * @var Operations
     */
    protected $operations;

    /**
     * @var Serializer
     */
    protected $serializer;

    public function __construct(SerializerInterface $serializer, Operations $operations)
    {
        $this->operations = $operations;
        $this->serializer = $serializer;
    }

    /**
     * @return ParkingList[]
     * @throws NonNumericValue
     * @throws NonStringUnitName
     * @throws ExceptionInterface
     */
    public function map(ParkingList $parkingList, Coordinate $coordinate): array
    {
        $mappedParkingList = [];
        foreach ($parkingList->getResults() as $key => $parking) {
            $destinationCoordinates = new Coordinate($parking->getLat(), $parking->getLng());

            $parkingNormalized = $this->serializer->normalize($parking, null, ['groups' => 'all']);
            /** @var Parking */
            $apiParking = $this->serializer->denormalize(
                $parkingNormalized,
                Parking::class,
                null,
                ['groups' => 'closest']
            );
            $apiParking->setDistanceInKilometers($this->operations->getVincentyDistance(
                $coordinate,
                $destinationCoordinates,
                'km'
            ));
            $mappedParkingList[$key] = $apiParking;
        }
        return $mappedParkingList;
    }
}
