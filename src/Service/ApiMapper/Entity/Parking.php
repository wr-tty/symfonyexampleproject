<?php

declare(strict_types=1);

namespace App\Service\ApiMapper\Entity;

use Symfony\Component\Serializer\Annotation\Groups;

class Parking
{
    /**
     * @var float
     * @Groups({"closest", "all"})
     */
    private $lat;

    /**
     * @var float
     * @Groups({"closest", "all"})
     */
    private $lng;

    /**
     * @var string
     * @Groups({"closest", "all"})
     */
    private $name;

    /**
     * @var int
     * @Groups({"closest", "all"})
     */
    private $numOfFreePlaces;

    /**
     * @var float
     * @Groups("all")
     */
    private $distanceInKilometers;

    public function getLat(): float
    {
        return $this->lat;
    }

    public function setLat(float $lat): void
    {
        $this->lat = $lat;
    }

    public function getLng(): float
    {
        return $this->lng;
    }

    public function setLng(float $lng): void
    {
        $this->lng = $lng;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getNumOfFreePlaces(): int
    {
        return $this->numOfFreePlaces;
    }

    public function setNumOfFreePlaces(int $numOfFreePlaces): void
    {
        $this->numOfFreePlaces = $numOfFreePlaces;
    }

    public function getDistanceInKilometers(): float
    {
        return $this->distanceInKilometers;
    }

    public function setDistanceInKilometers(float $distanceInKilometers): void
    {
        $this->distanceInKilometers = $distanceInKilometers;
    }
}
