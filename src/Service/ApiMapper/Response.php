<?php

declare(strict_types=1);

namespace App\Service\ApiMapper;

use App\Service\ApiMapper\Mappings\ClosestParking;
use App\Service\ApiMapper\Mappings\ListParking;
use App\Service\ApiMapper\Mappings\ListParkingCSV;
use App\Service\TskApi\Request;
use Location\Coordinate;
use PhpUnitsOfMeasure\Exception\NonNumericValue;
use PhpUnitsOfMeasure\Exception\NonStringUnitName;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class Response
{
    /**
     * @var ListParking
     */
    protected $listParking;

    /**
     * @var ClosestParking
     */
    private $closestParking;

    /**
     * @var ListParkingCSV
     */
    private $listParkingCSV;

    /**
     * @var Request
     */
    private $tskApiRequest;

    public function __construct(
        Request $tskApiRequest,
        ClosestParking $closestParking,
        ListParking $listParking,
        ListParkingCSV $listParkingCSV
    ) {
        $this->closestParking = $closestParking;
        $this->listParking = $listParking;
        $this->listParkingCSV = $listParkingCSV;
        $this->tskApiRequest = $tskApiRequest;
    }

    /**
     * Get all parking.
     * @throws ClientExceptionInterface
     * @throws NonNumericValue
     * @throws NonStringUnitName
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @throws ExceptionInterface
     */
    public function getAllParking(Coordinate $coordinate): string
    {
        $parkingList = $this->tskApiRequest->getParkings();
        return $this->listParking->getApiStructure($parkingList, $coordinate);
    }

    /**
     * Get all parking as CSV file.
     * @return bool|float|int|string
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getAllParkingCSV(Coordinate $coordinate): string
    {
        $parkingList = $this->tskApiRequest->getParkings();
        return $this->listParkingCSV->getApiStructure($parkingList, $coordinate);
    }

    /**
     * Return closest parking place.
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getClosestParking(Coordinate $coordinate): string
    {
        $parkingList = $this->tskApiRequest->getParkings();
        return $this->closestParking->getApiStructure($parkingList, $coordinate);
    }
}
