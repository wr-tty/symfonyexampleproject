<?php

declare(strict_types=1);

namespace App\Service\Utils;

class Sort
{
    public function sortArrayOfArraysByItemValue(
        array $arrayOfArrays,
        string $orderByItem = 'distanceInKilometers',
        string $orderType = 'ASC'
    ): array {
        usort($arrayOfArrays, function ($a, $b) use ($orderByItem, $orderType) {
            if (! isset($a[$orderByItem])) {
                throw new \InvalidArgumentException('Not Existing item key.');
            }
            if ($a[$orderByItem] === $b[$orderByItem]) {
                return 0;
            }
            if ($orderType === 'ASC') {
                return $a[$orderByItem] < $b[$orderByItem] ? -1 : 1;
            } elseif ($orderType === 'DESC') {
                return $a[$orderByItem] > $b[$orderByItem] ? -1 : 1;
            }
            throw new \InvalidArgumentException('Undefined $orderType parameter.');
        });
        return $arrayOfArrays;
    }

    public function sortArrayOfObjectsByPropertyValue(
        array $arrayOfObjects,
        string $orderByProperty = 'distanceInKilometers',
        string $orderType = 'ASC'
    ): array {
        usort($arrayOfObjects, function ($a, $b) use ($orderByProperty, $orderType) {
            $methodName = 'get' . ucfirst($orderByProperty);
            if (! method_exists($a, $methodName)) {
                throw new \InvalidArgumentException('Not Existing item key.');
            }
            if ($a->{$methodName}() === $b->{$methodName}()) {
                return 0;
            }
            if ($orderType === 'ASC') {
                return $a->{$methodName}() < $b->{$methodName}() ? -1 : 1;
            } elseif ($orderType === 'DESC') {
                return $a->{$methodName}() > $b->{$methodName}() ? -1 : 1;
            }
            throw new \InvalidArgumentException('Undefined $orderType parameter.');
        });
        return $arrayOfObjects;
    }
}
