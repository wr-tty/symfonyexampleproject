<?php

declare(strict_types=1);

namespace App\Service\Utils\Coordinates;

use Location\Coordinate;
use Location\Distance\Vincenty;
use PhpUnitsOfMeasure\Exception\NonNumericValue;
use PhpUnitsOfMeasure\Exception\NonStringUnitName;
use PhpUnitsOfMeasure\PhysicalQuantity\Length;

class Operations
{
    /**
     * Returning distance in kilometers
     * @throws NonNumericValue
     * @throws NonStringUnitName
     */
    public function getVincentyDistance(
        Coordinate $originCoordinates,
        Coordinate $destinationCoordinates,
        string $metricUnit
    ): float {
        $calculator = new Vincenty();
        $distanceInMeters = $calculator->getDistance($originCoordinates, $destinationCoordinates);
        $length = new Length($distanceInMeters, 'm');
        return $length->toUnit($metricUnit);
    }
}
