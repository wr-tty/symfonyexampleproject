<?php

declare(strict_types=1);

namespace App\EventSubscriber;

use App\Exception\UserVisibleException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;

final class ExceptionSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::EXCEPTION => ['onKernelException'],
        ];
    }

    public function onKernelException(ExceptionEvent $exceptionEvent): void
    {
        $exception = $exceptionEvent->getThrowable();

        if ($exception instanceof UserVisibleException) {
            $response = new JsonResponse([
                'data' => null,
                'error' => [
                    'code' => $exception->getCode(),
                    'message' => $exception->getMessage(),
                ],
            ], 400);
            $exceptionEvent->setResponse($response);
        } else {
            $errorCode = 123456;
            $response = new JsonResponse([
                'data' => null,
                'error' => [
                    'code' => $errorCode,
                    'message' => 'Error occurred.',
                ],
            ], 500);
            $exceptionEvent->setResponse($response);
        }
        // Some logging atc...
    }
}
